from Components.ActionMap import ActionMap
from Components.ChoiceList import ChoiceEntryComponent, ChoiceList
from Components.config import config, ConfigBoolean, ConfigNumber, ConfigSubsection, ConfigText
from Components.Label import Label
from Components.ScrollLabel import ScrollLabel
from Plugins.Plugin import PluginDescriptor
from Screens.EpgSelection import EPGSelection
from Screens.HelpMenu import HelpableScreen
from Screens.MessageBox import MessageBox
from Screens.Screen import Screen
from Tools.Directories import fileExists, resolveFilename, SCOPE_CONFIG
from twisted.internet import threads
from glob import glob
from gzip import GzipFile
from StringIO import StringIO
from time import localtime, strftime, strptime, time
from urllib2 import Request, urlopen
from .LCN import MetroLCN
import json
import os

try:
	from Plugins.Extensions.IMDb.plugin import IMDB
except:
	IMDB = None


__title__ = "YourTV info"
__version__ = "1.0.5.3"


config.plugins.yourtv = ConfigSubsection()
config.plugins.yourtv.metro = ConfigBoolean()
config.plugins.yourtv.region = ConfigNumber()
config.plugins.yourtv.timezone = ConfigText()


REGIONS = {
	"ACT": [
		("Canberra", 126, "Australia/Sydney"),
	],
	"NSW": [
		("Sydney", 73, "Australia/Sydney"),
		("Newcastle", 184, "Australia/Sydney"),
		("Wollongong", 71, "Australia/Sydney"),
		("South Coast", 259, "Australia/Sydney"),
		("Tamworth", 69, "Australia/Sydney"),
		("Central Coast", 66, "Australia/Sydney"),
		("Taree/Port Macquarie", 263, "Australia/Sydney"),
		("Lismore", 261, "Australia/Sydney"),
		("Coffs Harbour", 292, "Australia/Sydney"),
		("Orange/Dubbo", 262, "Australia/Sydney"),
		("Wagga Wagga", 264, "Australia/Sydney"),
		("Griffith", 67, "Australia/Sydney"),
		("Broken Hill", 63, "Australia/Broken_Hill"),
		("Remote and Central NSW", 106, "Australia/Sydney"),
	],
	"NT": [
		("Darwin", 74, "Australia/Darwin"),
		("Regional NT", 108, "Australia/Darwin"),
	],
	"QLD": [
		("Brisbane", 75, "Australia/Brisbane"),
		("Gold Coast", 78, "Australia/Brisbane"),
		("Sunshine Coast", 255, "Australia/Brisbane"),
		("Toowoomba", 256, "Australia/Brisbane"),
		("Wide Bay", 258, "Australia/Brisbane"),
		("Rockhampton", 254, "Australia/Brisbane"),
		("Mackay", 253, "Australia/Brisbane"),
		("Townsville", 257, "Australia/Brisbane"),
		("Cairns", 79, "Australia/Brisbane"),
		("Remote and Central QLD", 114, "Australia/Brisbane"),
	],
	"SA": [
		("Adelaide", 81, "Australia/Adelaide"),
		("Riverland", 83, "Australia/Adelaide"),
		("Spencer Gulf", 86, "Australia/Adelaide"),
		("Port Augusta", 82, "Australia/Adelaide"),
		("South East SA", 85, "Australia/Adelaide"),
		("Remote and Central SA", 107, "Australia/Adelaide"),
	],
	"TAS": [
		("Hobart", 88, "Australia/Hobart"),
		("Launceston", 293, "Australia/Hobart"),
	],
	"VIC": [
		("Melbourne", 94, "Australia/Melbourne"),
		("Geelong", 93, "Australia/Melbourne"),
		("Ballarat", 90, "Australia/Melbourne"),
		("Bendigo", 266, "Australia/Melbourne"),
		("Gippsland", 98, "Australia/Melbourne"),
		("Shepparton", 267, "Australia/Melbourne"),
		("Albury/Wodonga", 268, "Australia/Melbourne"),
		("Mildura/Sunraysia", 95, "Australia/Melbourne"),
	],
	"WA": [
		("Perth", 101, "Australia/Perth"),
		("Mandurah", 342, "Australia/Perth"),
		("Bunbury", 343, "Australia/Perth"),
		("Albany", 344, "Australia/Perth"),
		("Regional WA", 102, "Australia/Perth"),
	]
}

# REGIONNAMES = dict(itertools.chain.from_iterable((x[1], x[0]) for x in REGIONS.values()))
REGIONNAMES = {}
for x in REGIONS.values():
	for y in x:
		REGIONNAMES[y[1]] = y[0]


DUALREGIONS = {
	85: (90, {				# South East SA with Ballarat
		0x10100233, 		# ABC
		0x32020370, 		# SBS
		0x320C0802, 		# Nine
		0x3274327A, 		# WIN
		0x3283099D}),		# PRIME7

	90: (85, {				# Ballarat with South East SA
		0x10100253, 		# ABC
		0x32020390, 		# SBS
		0x3276327C, 		# WIN
		0x3282328A, 		# Seven
		0x3292329A}),		# Nine
}


# Map LCNs to the YourTV number (not currently needed).
YOURTVLCN = {
}


# Add long YELLOW to the EPG screens.

def yellowButtonPressedLong(self):
	self.closeEventViewDialog()
	from Screens.InfoBar import InfoBar
	InfoBarInstance = InfoBar.instance
	if InfoBarInstance.LongButtonPressed:
		cur = self['list'].getCurrent()
		begin = cur[0] and cur[0].getBeginTime() or self['list'].l.getCurrentSelection()[2]
		service = cur[1]
		self.session.open(YourTV, service.ref, begin)

def myEPGSelection__init__(self, *args, **kwargs):
	EPGSelection__init__(self, *args, **kwargs)
	self['colouractions'].actions['yellowlong'] = self.yellowButtonPressedLong
	self.helpList.append((self['colouractions'], 'ColorActions', [('yellowlong', _('YourTV info for current event'))]))

EPGSelection__init__ = EPGSelection.__init__
EPGSelection.__init__ = myEPGSelection__init__
EPGSelection.yellowButtonPressedLong = yellowButtonPressedLong


# Download an URL, with gzip compression/decompression.
def download(url):
	request = Request(url, headers={"Accept-Encoding": "gzip"})
	try:
		f = urlopen(request)
	except:
		return None
	if f.info().get("Content-Encoding") == "gzip":
		buf = StringIO(f.read())
		f = GzipFile(fileobj=buf)
	return f.read()


# Delete all existing guides older than six hours.
def remove_old_guides():
	old = time() - 6*60*60
	for f in glob("/tmp/YourTV-*.json"):
		if os.path.getmtime(f) < old:
			os.remove(f)


class YourTVRegion(Screen):
	skin = """
		<screen name="YourTVRegion" position="center,center" size="640,475" title="YourTV Region">
			<widget name="list" position="fill" />
		</screen>"""

	def __init__(self, session):
		Screen.__init__(self, session)

		self.expanded = []
		self["list"] = ChoiceList(self.getList())

		self["actions"] = ActionMap(["OkCancelActions"], {
			"ok": self.keyOk,
			"cancel": self.close,
		}, -1)

	def getList(self):
		regions = []
		for state in sorted(REGIONS):
			if state in self.expanded:
				regions.append(ChoiceEntryComponent('expanded', (state,)))
				for region in REGIONS[state]:
					regions.append(ChoiceEntryComponent('verticalline', region))
			else:
				regions.append(ChoiceEntryComponent('expandable', (state,)))
		return regions

	def keyOk(self):
		item = self["list"].l.getCurrentSelection()[0]
		if len(item) == 1:
			if item[0] in self.expanded:
				self.expanded.remove(item[0])
			else:
				self.expanded.append(item[0])
			self["list"].setList(self.getList())
		else:
			config.plugins.yourtv.metro.value = False
			config.plugins.yourtv.region.value = item[1]
			config.plugins.yourtv.timezone.value = item[2]
			config.plugins.yourtv.save()
			self.close()


class YourTV(Screen):
	skin = """
		<screen name="YourTV" position="center,center" size="640,475" title="YourTV info">
			<widget name="info" position="10,0" size="620,430" font="Regular;22" />
			<widget name="statusbar" position="10,430" size="620,18" font="Regular;16" foregroundColor="#cccccc" />
			<ePixmap position="20,e-20" size="15,16" pixmap="skin_default/buttons/button_red.png" alphatest="blend" />
			<ePixmap position="340,e-20" size="15,16" pixmap="skin_default/buttons/button_yellow.png" alphatest="blend" />
			<widget name="key_red" position="40,e-25" size="150,25" valign="center" halign="left" font="Regular;20" />
			<widget name="key_yellow" position="360,e-25" size="150,25" valign="center" halign="left" font="Regular;20" />
		</screen>"""

	def __init__(self, session, service, eventBegin):
		Screen.__init__(self, session)

		self["info"] = ScrollLabel()
		self["statusbar"] = Label()
		self["key_red"] = Label()
		self["key_yellow"] = Label()

		self["actions"] = ActionMap(["OkCancelActions", "DirectionActions", "ColorActions", "MenuActions", "HelpActions"], {
			"ok": self.close,
			"cancel": self.close,
			"menu": self.openRegion,
			"left": self["info"].pageUp,
			"right": self["info"].pageDown,
			"up": self["info"].pageUp,
			"down": self["info"].pageDown,
			"red": self.keyRed,
			"yellow": self.keyYellow,
			"displayHelp": self.displayHelp,
		}, -1)

		self.name = None
		self.imdbId = None
		self.region = None

		self.lcn = self.getLCN(service)
		if not self.lcn:
			self["statusbar"].text = _("Unable to determine LCN")
			return

		now = time()
		nowtm = localtime(now)
		self.begtm = localtime(eventBegin)
		self.day = strftime("%a", self.begtm)
		if self.begtm.tm_mday != nowtm.tm_mday:
			if eventBegin < now:
				self.day = "yesterday"
			elif self.begtm.tm_wday == nowtm.tm_wday:
				self["statusbar"].text = _("YourTV does not provide next week")
				return

		if config.plugins.yourtv.region.value:
			self.start()
		else:
			self.onFirstExecBegin.append(self.getRegion)

	def openRegion(self):
		self.old_region = config.plugins.yourtv.region.value
		self.session.openWithCallback(self.newRegion, YourTVRegion)

	def newRegion(self):
		if config.plugins.yourtv.region.value != self.old_region:
			self.start()

	def getRegion(self):
		self.session.openWithCallback(self.gotRegion, YourTVRegion)

	def gotRegion(self):
		if config.plugins.yourtv.region.value:
			self.start()
		else:
			self["statusbar"].text = _("No region set")

	def query(self, api, callback):
		threads.deferToThread(download, "https://www.yourtv.com.au/api/" + api).addCallback(callback)

	def start(self):
		remove_old_guides()
		self["info"].setText("")
		self["statusbar"].text = ""
		self.region = config.plugins.yourtv.region.value
		if self.region in DUALREGIONS and self.network in DUALREGIONS[self.region][1]:
			self.region = DUALREGIONS[self.region][0]
		if config.plugins.yourtv.metro.value:
			region = MetroLCN[self.region][0]
			self["key_yellow"].text = _("Regional")
		else:
			region = self.region
			self["key_yellow"].text = region in MetroLCN and _("Metro") or ""
		self.setTitle("%s - %s" % (__title__, REGIONNAMES[region]))
		self.guidefile = "/tmp/YourTV-%s-%s.json" % (strftime("%F", self.begtm), region)
		if fileExists(self.guidefile):
			f = open(self.guidefile)
			guide = f.read()
			f.close()
			self.gotGuide(guide)
		else:
			self["statusbar"].text = _("Retrieving YourTV guide for %s...") % self.day
			self.query("guide/?day=%s&timezone=%s&region=%s&format=json" % (self.day.lower(), config.plugins.yourtv.timezone.value, region), self.gotGuide)

	def gotGuide(self, guide):
		if not guide:
			self["statusbar"].text = _("Unable to download guide")
			return

		try:
			f = open(self.guidefile, "w")
			f.write(guide)
			f.close()
		except:
			pass

		if config.plugins.yourtv.metro.value and self.lcn in MetroLCN[self.region][1]:
			lcn = MetroLCN[self.region][1][self.lcn]
		else:
			lcn = self.lcn
			if self.region in YOURTVLCN and lcn in YOURTVLCN[self.region]:
				lcn = YOURTVLCN[self.region][lcn]
			# Apart from Darwin, switch 10 HD on LCN 1 to YourTV 15.
			elif self.region != 74:
				if lcn == 1:
					lcn = 15
		show = self.getId(guide, lcn, self.begtm)
		if not show:
			return
		self["statusbar"].text = _("Retrieving show info...")
		self.query("airings/" + str(show), self.gotShow)

	def gotShow(self, show):
		if not show:
			self["statusbar"].text = _("Unable to download info")
			return
		try:
			show = json.loads(show)
		except:
			self["statusbar"].text = _("Unable to read info")
			return
		self["statusbar"].text = ""

		if show['movie']:
			label = _("Movie")
		elif show['series']:
			label = _("Series")
		else:
			label = _("Name")
		self.name = title = show['title']
		if show['program']['yearReleased']:
			title += " (%s)" % show['program']['yearReleased']
		self.addEntry(label, title)
		if show['episodeTitle']:
			title = show['episodeTitle']
			if show['seriesNumber'] and show['episodeNumber']:
				title += _(" (S%s E%s)") % (show['seriesNumber'], show['episodeNumber'])
			elif show['seriesNumber']:
				title += _(" (S%s)") % show['seriesNumber']
			elif show['episodeNumber']:
				title += _(" (E%s)") % show['episodeNumber']
			self.addEntry(_("Title"), title)
		else:
			self.addEntry(_("Season"), show['seriesNumber'])
			self.addEntry(_("Episode"), show['episodeNumber'])
		notes = []
		if show['newEpisode']: notes += [_("New")]
		if show['premiere']: notes += [_("Premiere")]
		if show['repeat']: notes += [_("Repeat")]
		if show['final']: notes += [_("Final")]
		if show['seriesReturn']: notes += [_("Return")]
		if show['subtitles'] or show['closedCaptions']: notes += [_("Subtitles")]
		self.addEntry(_("Notes"), ", ".join(notes))
		if show['yearProduction'] != show['program']['yearReleased']:
			self.addEntry(_("Year"), show['yearProduction'])
		if show['classification']:
			rating = show['classification']
			if show['warnings']:
				rating += "(%s)" % show['warnings']
			self.addEntry(_("Rating"), rating)
		else:
			self.addEntry(_("Warnings"), show['warnings'])
		self.addEntry(_("Director"), show['director'])
		self.addEntry(_("Country"), show['country'])
		if show['genre']:
			genre = show['genre']['name']
			if show['subGenre']:
				genre += " (%s)" % show['subGenre']['name']
			self.addEntry(_("Genre"), genre)
		s1 = show['synopsis']
		s2 = show['program']['shortSynopsis']
		s3 = show['program']['mediumSynopsis']
		s4 = show['program']['longSynopsis']
		self.addEntry(_("Synopsis"), s1, True)
		self.addEntry(_("Cast"), show['mainCast'], True)
		if s2 != s1:
			self.addEntry(_("Short synopsis"), s2, True)
		if s3 not in (s1, s2):
			self.addEntry(_("Medium synopsis"), s3, True)
		if s4 not in (s1, s2, s3):
			self.addEntry(_("Long synopsis"), s4, True)

		if IMDB:
			self.imdbId = show['program']['imdbId']
			self["key_red"].text = self.imdbId and "IMDb" or _("IMDb search")

	def addEntry(self, label, value, separate=False):
		if value:
			if isinstance(value, unicode):
				value = value.encode("utf-8")
			if separate:
				label = "\n" + label
			self["info"].appendText("%s:%s%s\n" % (label, separate and "\n" or " ", value), False)

	def keyRed(self):
		if IMDB and self.name:
			if self.imdbId:
				self.session.open(IMDB, self.name, imdbId="tt" + self.imdbId)
			else:
				self.session.open(IMDB, self.name)

	def getLCN(self, service):
		namespace = service.getUnsignedData(4)
		nid = service.getUnsignedData(3)
		tsid = service.getUnsignedData(2)
		sid = service.getUnsignedData(1)
		ref = "%08x:%04x:%04x:%04x:" % (namespace, nid, tsid, sid)
		try:
			f = open(resolveFilename(SCOPE_CONFIG, "lcndb"))
		except Exception, e:
			print "[YourTV]", e
			return

		self.network = nid << 16 | tsid

		lcn = 0
		for line in f:
			if line.startswith(ref):
				lcn = int(line[len(ref):len(ref)+5])
				break
		f.close()
		return lcn

	def getId(self, guide, lcn, begtm):
		try:
			yourtv = json.loads(guide)
		except:
			self["statusbar"].text = _("Unable to read guide (%s)") % self.guidefile
			return
		channel = [x for x in yourtv[0]['channels'] if x.get('number') == lcn]
		if len(channel) != 1:
			self["statusbar"].text = channel and _("More than one LCN!") or _("Unable to find LCN")
			return

		begin = begtm.tm_hour * 60 + begtm.tm_min + 86400
		prev = 0
		show_id = None
		for block in channel[0]['blocks']:
			offset = 0 if "yesterday" in block['classes'] else 86400
			for show in block['shows']:
				start = strptime(show['date'], "%I:%M %p")
				start = start.tm_hour * 60 + start.tm_min + offset
				if start == begin:
					return show['id']
				if start > begin:
					return start - begin < begin - prev and show['id'] or show_id
				prev = start
				show_id = show['id']
		if not show_id:
			self["statusbar"].text = _("Show not found")
		return show_id

	def keyYellow(self):
		if self.lcn and self.region in MetroLCN:
			config.plugins.yourtv.metro.value ^= True
			config.plugins.yourtv.metro.save()
			self.start()

	def displayHelp(self):
		self.session.open(MessageBox, _(
			"%s (v%s)\n"
			"\n"
			"Retrieve show information from yourtv.com.au, based on LCN and time.\n"
			"\n"
			"The name contains year of release; the entry is year of production.\n"
			"\n"
			"IMDb: view IMDb info for this show\n"
			"IMDb search: search IMDb for this show (using the YourTV name, not EPG)\n"
			"Metro: switch to the metropolitan guide, which may provide more information\n"
			"Regional: switch back to your original location") % (__title__, __version__),
			MessageBox.TYPE_INFO)


def eventinfo(session, service=None, event=None, **kwargs):
	if service is None:
		service = session.nav.getCurrentService()
		if service:
			info = service.info()
			event = info.getEvent(0) # 0 = now, 1 = next
			service = session.nav.getCurrentlyPlayingServiceReference()
	else:
		service = service.ref
		if '0:0:0:0:0:0:0:0:0' in service.toString():
			session.open(MessageBox, _("Recordings are not supported."), MessageBox.TYPE_ERROR, timeout=5)
			return
	if event:
		session.open(YourTV, service, event.getBeginTime())


def Plugins(**kwargs):
	return PluginDescriptor(name=__title__,
							description="Retrieve show information from YourTV",
							where=PluginDescriptor.WHERE_EVENTINFO,
							fnc=eventinfo,
							needsRestart=False)
